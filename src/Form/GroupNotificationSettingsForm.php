<?php

namespace Drupal\group_notifications\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Group notification settings form.
 */
class GroupNotificationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'group_notifications.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_notificationsettings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $values = [];
    $values['member'] = t('Notification to new member after successfully adding to the group.');
    $values['owner'] = t('Notification to group owner when a member joins the group(using Join group).');
    $values['leave'] = t('Notification to the group owner when a member exit the group.');
    $values['new_post'] = t('Notification to the group members when a member posts/adds any content in the group.');

    $config = $this->config('group_notifications.adminsettings');

    $form['notify_options'] = [
      '#type' => 'checkboxes',
      '#title' => t('Enable the type of notification required in group.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 10,
      '#options' => $values,
      '#default_value' => $config->get('notify_options'),

    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('group_notifications.adminsettings')
      ->set('notify_options', $form_state->getValue('notify_options'))
      ->save();
  }

}
