<?php

namespace Drupal\group_notifications\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes Node Tasks.
 *
 * @QueueWorker(
 *   id = "group_notification_queue_processor",
 *   title = @Translation("Task Worker: Send mail"),
 *   cron = {"time" = 10}
 * )
 */
class GroupNotificationQueueProcessor extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!empty($data)) {
      $to_mail = $data->to_mail;
      $group_title = $data->group_title;
      $param = $data->param;
      $condition = $data->condition;
      $status = $data->status;
      _group_notifications_send_mail($to_mail, $group_title, $param, $condition, $status);
    }
  }

}
