<?php

namespace Drupal\group_notifications\Plugin\QueueWorker;

/**
 * A Node Publisher that publishes nodes on CRON run.
 *
 * @QueueWorker(
 *   id = "cron_queue_processor",
 *   title = @Translation("Cron Queue Processor"),
 *   cron = {"time" = 10}
 * )
 */
class CronQueueProcessor extends GroupNotificationQueueProcessor {}
