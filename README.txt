CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Pre-Requirements
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provides the notification functionality for the members of group 
in drupal site. We have given the following notifications types:
* Notification to new member after successfully adding to the group.
* Notification to group owner when a member joins the group(Using Join group).
* Notification to group owner when a member lefts the group.
* Notification to the group members when a member post any content in the group.

PRE-REQUIREMENTS
------------
1)Entity, VariationCache, 
2)Group, Group Node
3)SMTP authentication support 
4)PHPmailer

REQUIREMENTS
------------

This module requires the following:
1)Drupal 8.8^
2)Install SMTP authentication support module for mailing purpose.
  Also use composer to install them which will install all the 
  dependencies like Phpmailer library.

INSTALLATION
------------

1) Install as usual,
   see https://www.drupal.org/docs/8/extending-drupal-8/
   for further information.


CONFIGURATION
-------------

After successfully installing the module, 
go to the url "/admin/config/group_notifications/adminsettings"
There you can see checkboxes under label 
“Enable the type of notification required in group.”

MAINTAINERS
-----------

Sana Shamsudin
https://www.drupal.org/user/3647271

Dileepraj PR
https://www.drupal.org/user/3606333

Elizabeth Amanda
https://www.drupal.org/user/3656577

Meera Leela
https://www.drupal.org/u/Meerachandran

Riyas KN
https://www.drupal.org/u/riyaskn
